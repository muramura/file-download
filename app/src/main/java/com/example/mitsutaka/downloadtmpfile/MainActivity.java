package com.example.mitsutaka.downloadtmpfile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        Button DownloadButton = (Button) findViewById(R.id.downloadbutton);
        Button CalculationButton = (Button) findViewById(R.id.calculation);

        DownloadButton.setOnClickListener(this);
        CalculationButton.setOnClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void onClick(View v) {
        // ボタン1が押された場合
        if (v.getId() == R.id.downloadbutton) {
            Toast.makeText(this, "ボタン1が押されました！", Toast.LENGTH_LONG).show();
            //ファイルのダウンロード
            Uri uri = Uri.parse("http://n225cme.com/data/ni225d_5min.dat?t=1438588755362");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (v.getId() == R.id.calculation) {
            Toast.makeText(this, "ボタンcalculationが押されました！", Toast.LENGTH_LONG).show();
            FileOpen();
        } else {
            Log.d("debug", "error");
        }
    }


    //ファイルの展開･計算できる形に直す
    public void FileOpen() {
        Log.d("InFile", "in!");
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
//            参考サイト
//            http://tiro105.hateblo.jp/entry/2015/03/22/232708


            File file = new File("ni225d_min.dat");
            //String fileName=file.getName();
            //fileName= fileName.replaceFirst("\\..*",".txt");
            Log.d("InFile", String.valueOf(file));
            try {
                String fileName = String.valueOf(file);
                String encodingName = "Shift_JIS";
                InputStreamReader isr = new InputStreamReader(openFileInput(fileName), encodingName);
                reader = new BufferedReader(isr);
                //reader = new BufferedReader(new InputStreamReader(openFileInput(fileName), "UTF-8"));

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("InFile", "error");
            }
            while (reader.readLine() != null) {
                Log.d("InFile", reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
